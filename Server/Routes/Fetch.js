const express = require('express');
const Fetch = express.Router();
const ProductModel = require('../Model/Fetchmodel'); // Update the path as needed

Fetch.get('/', async (req, res) => {
    try {
        // Use Mongoose to find all products
        const products = await ProductModel.find({});

        if (products.length > 0) {
            res.send(products);
        } else {
            res.send("No data found");
        }
    } catch (error) {
        console.error(error);
        res.status(500).send("Internal Server Error");
    }
});

module.exports = Fetch;
