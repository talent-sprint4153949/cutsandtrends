const express = require('express');
const Appointment= express.Router();
const Appointmentmodel = require('../Model/Appomodel'); // Update the path as needed

Appointment.post('/', async (req, res) => {
    try {
        const appointmentData = {
            "title": req.body.title,
            "date": parseInt(req.body.date),
            "time": parseInt(req.body.time),
            "email": req.body.email,
            "number": parseInt(req.body.number),
            "address": req.body.address,
            "name": req.body.name,
        };

        // Create a new document using the Mongoose model
        const result = await Appointmentmodel.create(appointmentData);

        res.send("Data successfully inserted: ");
    } catch (error) {
        console.error(error);
        res.status(500).send("Internal Server Error");
    }
});

module.exports = Appointment;
