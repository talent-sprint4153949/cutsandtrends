// models/LoginModel.js

const mongoose = require('mongoose');
if(!mongoose.models['details']){
const loginSchema = new mongoose.Schema({
  Email: {
    type: String,
    required: true,
  },
  Password: {
    type: String,
    required: true,
  },
});
mongoose.model('details',loginSchema)
}

const Loginmodel = mongoose.model('details');

module.exports = Loginmodel;
