const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
    id: {
        type: Number,
        required: true,
    },
    img: {
        type: String,
        required: true,
    },
    title: {
        type: String,
        required: true,
    },
    price: {
        type: String, // Assuming the price is stored as a string, adjust if needed
        required: true,
    },
});

const Productmodel = mongoose.model('Products', productSchema);

module.exports = Productmodel;
