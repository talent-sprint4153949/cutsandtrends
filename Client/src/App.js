
import {BrowserRouter,Routes,Route} from 'react-router-dom'
import NavBar from './components/NavBar';
import Home from './pages/Home';
import Services from './pages/Services';
import Shop from './pages/Shop';
import Aboutus from './pages/Aboutus';
import Register from './pages/Register';
import Login from './pages/Login';
import Cart from './pages/Cart'





function App() {
  return (
    <div>
      <BrowserRouter>
     <NavBar />
      <div className='main-content'>
      <Routes>
        <Route path="/" element ={<Home />}/>
        <Route  path='/Services' element={<Services />}/>
        <Route  path='/Shop' element={<Shop />}/>
        <Route path='/About' element={<Aboutus />} />
        <Route  path='/Addcart' element={<Cart />}/>
        <Route path='/Register' element={<Register />} />
        <Route path='/Login' element={<Login />} />
       
      </Routes>
      </div>
      </BrowserRouter>
    </div>
  );
}

export default App;
