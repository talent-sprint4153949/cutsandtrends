import React from 'react';
import Footer from '../components/Footer'
import './cutsandtrends.css';
import { Row, Col } from 'reactstrap'; 


const Aboutus = () => {
  return (
    <div>
    <div className="body" style={{ objectFit: "cover" ,backgroundColor:"#2c2c2c" }}>
      <section  className="section">
        <Row  style={{width:"100%"}}className="history">
          <h1 className="h1">PROFESSIONAL SALON FOR MEN ONLY.</h1>
          <Col xs="12" md="4" sm="6" lg="3" className="item-2">
            <h3>Since 2015</h3>
            <p>We started our journey.</p>
          </Col>
          <Col xs="12" md="4" sm="6" lg="3" className="item-3">
            <h3>2000+ CLIENTS</h3>
            <p>Worldwide customers.</p>
          </Col>
        </Row>
        <div className="about-details">
          <img src="https://toniandguysalon.in/wp-content/uploads/2022/11/salon-min-1.jpg" alt="" />
          <div className="about-paragraph">
            <p>We have neat and clean places for your arrival and a pleasant environment.</p>
            <p>Our professionals provide the best haircuts to meet your expectations.</p>
          </div>
        </div>
        <div className="meet-the-masters">
          <h1 className="h1">MEET THE MASTERS</h1>
        </div>
        <div className="shop-masters">
          <div className="master">
            <img src='https://www.judesbarbershop.com/wp-content/uploads/2021/03/Barber-Shop-Haircut-460x460.jpg.webp' alt='' className="imgm1" />
            <h3 className="h3">Mr. John</h3>
            <p>Top barber</p>
          </div>
          <div className="master">
            <img src="https://assets-global.website-files.com/5cb569e54ca2fddd5451cbb2/6475e8e443b0b0dbb5087334_Johhny-Tamworth.jpg" alt='' className="imgm2" />
            <h3 className="h3">Mr. Joseph</h3>
            <p>Top barber</p>
          </div>
          <div className="master">
            <img src='https://img.freepik.com/premium-photo/face-professional-handsome-bearded-barber-holding-trimmer-smiling-happily-posing-his-barbershop_54255-54.jpg?w=2000' alt='' className="imgm3" />
            <h3 className="h3">Mr. Sean</h3>
            <p>Top barber</p>
          </div>
        </div>
      </section>
      <section className="our-works">
        <h1 className="shop-head-works">OUR WORKS</h1>
        <div className="our-works-images">
          <img src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSz8Fbi3ya6HORiNj0vCkSD-Q8chLIf_IXOwg&usqp=CAU' alt='' />
          <img src='https://st2.depositphotos.com/5444644/8320/i/950/depositphotos_83202440-stock-photo-barber-cleaning-face-of-his.jpg' alt=''/>
            <img src='https://img.freepik.com/premium-photo/master-washes-head-client-barber-shop-hairdresser-makes-hairstyle-young-man_124865-3162.jpg' alt='' class="img3" />
            <img src='https://thumbs.dreamstime.com/b/barber-shaving-man-beard-straight-razor-barbershop-close-up-hairdresser-hands-sterile-gloves-using-cut-throat-199676875.jpg' alt=''/>
        </div>
      </section>
      <section className="about-us">
        <div className="clients-comment">
          <h1 className="h1">WHAT OUR CLIENTS SAY ABOUT US.</h1>
          <div className="clients-comment-paragraphs">
            <div className="clients-comment-paragraph">
              <img src='https://t4.ftcdn.net/jpg/03/26/98/51/360_F_326985142_1aaKcEjMQW6ULp6oI9MYuv8lN9f8sFmj.jpg' alt='user-1' />
              <div className="main-paragraph">
                <p>The service provided is excellent, and the professionals work at their best.</p>
                <h6>JUSTIN PILL</h6>
              </div>
            </div>
            <div className="clients-comment-paragraph">
              <img src='https://t4.ftcdn.net/jpg/02/14/74/61/360_F_214746128_31JkeaP6rU0NzzzdFC4khGkmqc8noe6h.jpg' alt='user-2' />
              <div className="main-paragraph">
                <p>Their service is top-notch, using natural products for your hair, which are also available.</p>
                <h6>BOBBY</h6>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="ready-to-transform">
        <h1>READY TO TRANSFORM?</h1>
        <p>Contact usday to transform yourself to meet your expectations.</p>
        <a href='"mailto:name@email.com"' className="second-btn" >Contact us</a>
      </section>
    </div>
    <Footer />
    </div>
  );
};

export default Aboutus;

