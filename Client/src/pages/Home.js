import React from 'react'
import Carousell from '../components/Carousell'
import ShortInfo from '../components/ShortInfo'
import OurGallery from '../components/OurGallery'
import Footer from '../components/Footer'

export default function Home() {
  return (
    <div style={{backgroundColor:"rgb(26,24,24)", height:"300%"}}>
   <div> <Carousell /></div>
    <div> <ShortInfo /></div>
     <div> <OurGallery  /></div>
    <div> <Footer /></div>
    </div>
  )
}
 