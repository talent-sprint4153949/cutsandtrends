import React from 'react';
import { Container,Row,Col } from 'reactstrap';
import { FaFacebook,  FaInstagram, FaLinkedin, FaTwitter, } from 'react-icons/fa'; // Import icons as needed

const Footer = () => {
  return (
    <footer className="bg-dark text-light" style={{justifyContent:'center',alignItems:'center'}}>
    <Container className="py-4 h-60 p-7  w-100 container bg-dark mx-md-4">
        <Row>
          <Col md={6}>
            <h5 style={{color:'#dc3545',fontSize:'23px'}}>About Us</h5>
            <p>
             
"Cuts & Trends Salon: Where Style Meets Innovation"

Welcome to Cuts & Trends, where we redefine beauty with the latest trends and cutting-edge styles. Our salon is not just about haircuts; it's a sanctuary for transformation and self-expression.
            </p>
          </Col>
          <Col md={3} >
            <h5 style={{color:"#dc3545", marginTop:"5px",fontSize:'23px' }}>Connect with Us</h5>
            <Row className="justify-content-start">
              <Col className="text-light me-3">
                <FaFacebook />
              </Col>
              <Col className="text-light me-3">
                <FaInstagram />
              </Col>
              <Col className='text-light me-3'>
                <FaTwitter />
              </Col>
              
              <Col className="text-light me-3">
                <FaLinkedin />
              </Col>
            </Row>
          </Col>
          <Col md={3}  >
            <h5 style={{color:"#dc3545", paddingLeft:"10px",fontSize:'23px'}} >Useful Links</h5>
            <ul className="list-unstyled" style={{paddingLeft:"10px"}}>
              <li><a href="/" className="text-light">Home</a></li>
              <li><a href='/Services' className="text-light">Services</a></li>
              <li><a href="/Our" className="text-light">Gallery</a></li>
              <li><a href="/Aboutus" className="text-light">Contact</a></li>
            </ul>
          </Col>
        </Row>
      </Container>
      <div className="text-center p-3 bg-secondary">
        © {new Date().getFullYear()} CUTS & TRENDS
      </div> 

    </footer>
  );
};

export default Footer;
