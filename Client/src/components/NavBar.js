import React, { useState } from 'react';
import '@fortawesome/fontawesome-free/css/all.css';

import { Link, useNavigate } from 'react-router-dom';
import {
  Navbar,
  NavbarBrand,   
  NavbarToggler,
  Collapse,
  Nav,
  NavItem,
  NavLink,
} from 'reactstrap';
import { useSelector } from 'react-redux';

const NavBar = () => {
  const [isOpen, setIsOpen] = useState(false);
  const navigate = useNavigate();
  const auth = localStorage.getItem('token');
  const cartProducts = useSelector(state=>state.cart);

  const toggle = () => {
    setIsOpen(!isOpen);
  };

  const logout = () => {
    localStorage.removeItem('token');
    navigate('/');
  };
  

  return (
    <div  style={{ backgroundColor: '#2C2C2C',  display:'flex' }}>
      <Navbar dark expand="md" className='d-flex justify-content-between' > 
        <NavbarBrand >
           <NavLink to='/Home'> <img style={{  height:'50px',width:'250px', display:"flex",alignItems:"center" }} src={require('../Images/Cuts & Trends.png')} alt="Logo" /></NavLink>
        </NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="ml-auto " navbar style={{float:'right',fontSize:'16  px',fontWeight:"bold"}} >
            {auth ? (
              <>
                <NavItem style={{float:'right'}} >
                  <NavLink style={{color:"white",paddingRight:'20px'}}  tag={Link} to="/">Home</NavLink>
                </NavItem> 
                <NavItem>
                  <NavLink   style={{color:'white'}}  tag={Link} to="/Services">Services</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink  style={{color:'white'}} tag={Link} to="/Shop">Shop</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink   style={{color:'white'}}tag={Link} to="/About">About us</NavLink>
                </NavItem>
                
                <NavItem  className='me-auto' >
                  <NavLink   style={{color:'whitesmoke'}}tag={Link} to="/Addcart"><i class="fa-solid fa-cart-shopping">{cartProducts.length}</i></NavLink>
                </NavItem>
               
                <NavItem>
                  <NavLink   style={{color:'white'}}onClick={logout}>Logout</NavLink>
                </NavItem>
               
              
              </>
            

            ) : (
              <>
                <NavItem>
                  <NavLink  style={{color:'white'}} tag={Link} to="/">Home</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink   style={{color:'white'}}tag={Link} to="/About">About us</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink   style={{color:'white'}} tag={Link} to="/Register">Register</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink   style={{color:'white'}}   tag={Link} to="/Login">Login</NavLink>
                </NavItem>
              </>
            )}
          </Nav>
        </Collapse>
        
      </Navbar>
    </div>
  );
};

export default NavBar;





