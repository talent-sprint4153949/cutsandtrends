import React from 'react';
import {Container,Row,Col} from 'reactstrap'


const OurGallery = () => {

    return (
        <div  >
            <Container className='mt-5 mb-5 justify-content-center' style={{alignItems:"baseline",  background:"#2c2c2c"}}>
                <h1 className="mb-4" style={{color:'white', paddingLeft:"20px", textAlign:'center',paddingTop:'20px'}}>GALLERY WITH OUR CUTS</h1>
                <Row  md="6" lg="3" xl="3" style={{ display:"flex",justifyContent:"center",alignItems:"center", padding:" 0px 30px" ,marginLeft:"10px"}}>

                        <Col   md="6" className="mb-4 justifyContent-center" >
                        <img src={require('../services/hair-cut-1.jpg')}  alt="face-wash-1"  style={{ width: '300px', height: '250px'}} />
                        </Col>
                        <Col   md='6'  className="mb-4"  >
                        <img src={require('../services/BREADTRIM.jpg')} alt="hair-cut-1"  style={{ width: '300px', height: '250px' }} />
                        </Col>
                        <Col   md="6" className="mb-4" >
                        <img src={require('../services/SHAVE (1).jpg')} alt="hair-dyeing-1"   style={{ width: '300px', height: '250px' }}/>
                        </Col>
                        <Col   md="6" className="mb-4" >
                        <img src={require('../services/hair-wash-5.jpg')} alt="hair-wash-1"  style={{ width: '300px', height: '250px' }} />
                        </Col>
                        <Col  md="6"  className="mb-4">
                        <img src={require('../services/face-wash-4.jpg')} alt="shave-ima-1"   style={{ width: '300px', height: '250px' }}/>
                        </Col>
                        <Col   md="6" className="mb-4"  >
                        <img src={require('../services/hair-straight-2.jpg')} alt="face-wash-2"  style={{ width: '300px', height: '250px' }} />
                        </Col>
                        <Col   md="6" className="mb-4"  >
                        <img src={require('../services/hair-cut-4.jpg')} alt="hair-cut-2"   style={{ width: '300px', height: '250px' }}/>
                        </Col>
                        <Col   md="6" className="mb-4" >
                        <img src={require('../services/shave-image-2.jpg')} alt="hair-dyeing-2"   style={{ width: '300px', height: '250px' }}/>
                        </Col>
                        <Col   md="6" className="mb-4" >
                        <img src={require('../services/hair-wash-4.jpg')} alt="hair-wash-2"  style={{ width: '300px', height: '250px' }} />
                        </Col>
                        <Col   md="6"className="mb-4"  >
                        <img src={require('../services/face-wash-1.jpg')} alt="shave-ima-2"  style={{ width: '300px', height: '250px' }} />
                        </Col>
                        <Col   md="6" className="mb-4"  >
                         
                        <img src={require('../services/hair-straight-1.jpg')} alt="face-wash-3.jpg"   style={{ width: '300px', height: '250px' }}/>
                        </Col >
                        <Col   md="6"  className="mb-4" >
                        <img src={require('../services/face-wash-5.jpg')}alt="hair-cut-3.jpg"   style={{ width: '300px', height: '250px' }}/>
                        </Col>
                        <Col   md="6" className="mb-4" >
                        <img src={require('../services/hair-wash-1.jpg')} alt="hair-dyeing-3.jpg"  style={{ width: '300px', height: '250px' }} />
                        </Col>
                        <Col  md="6" className="mb-4" >
                        <img src={require('../services/shave-image-2.jpg')} alt="hair-wash-3.jpg"   style={{ width: '300px', height: '250px' }}/>
                        </Col>
                        <Col  md="6" className="mb-4">
                        <img  src={require('../services/hair-dyeing-2.jpg')} alt='hairdying' style={{width:'300px', height:"250px"}}/>
                        </Col>
                        <Col md="6" className="mb-4" >
                        <img  src={require('../services/hair-wash-2.jpg')} alt='hairwash' style={{width:'300px', height:"250px"}}/>
                        </Col>
                        <Col md="6" className="mb-4" >
                        <img  src={require('../services/hair-cut-6.jpg')} alt='haircut' style={{width:'300px', height:"250px"}}/>
                        </Col>
                        <Col md="6" className="mb-4" >
                        <img  src={require('../services/shave-image-6.jpg')} alt='shave' style={{width:'300px', height:"250px"}}/>
                        </Col>
                        </Row>


            
            </Container>
            


        </div>
    );
};

export default OurGallery;